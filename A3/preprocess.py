from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
import time
from scipy.misc import imread
from scipy.misc import imresize
from scipy.misc import imsave
import matplotlib.image as mpimg
import os
from scipy.ndimage import filters
import urllib


#male = list(set([a.split("\t")[0] for a in open("subset_actors.txt").readlines()]))
#female = list(set([a.split("\t")[0] for a in open("subset_actresses.txt").readlines()]))
enum = {'Gerard Butler':0,'Daniel Radcliffe':1, 'Michael Vartan':2, 'Lorraine Bracco':3, 'Peri Gilpin':4, 'Angie Harmon':5}


def timeout(func, args=(), kwargs={}, timeout_duration=1, default=None):
    '''From:
    http://code.activestate.com/recipes/473878-timeout-function-using-threading/'''
    import threading
    class InterruptableThread(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.result = None

        def run(self):
            try:
                self.result = func(*args, **kwargs)
            except:
                self.result = default

    it = InterruptableThread()
    it.start()
    it.join(timeout_duration)
    if it.isAlive():
        return False
    else:
        return it.result

def rgb2gray(rgb):
    '''Return the grayscale version of the RGB image rgb as a 2D numpy array
    whose range is 0..1
    Arguments:
    rgb -- an RGB image, represented as a numpy array of size n x m x 3. The
    range of the values is 0..255
    '''

    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
    
def download(act, chopped=True, num=120):
    testfile = urllib.URLopener()      
    f = open('uncropped/track.txt', 'r+')
    names = f.readline()
    f.close()
    all_names = names.split(',')
    #Note: you need to create the uncropped folder first in order 
    #for this to work
    
    for a in act:
        print a
        name = a.split()[1].lower()
        if name in all_names:
            continue
        i = 0
        for line in open("faces_subset.txt"):
            if a in line and i < num:
                filename = name+str(i)+'.'+line.split()[4].split('.')[-1]
                #A version without timeout (uncomment in case you need to 
                #unsupress exceptions, which timeout() does)
                #testfile.retrieve(line.split()[4], "uncropped/"+filename)
                #timeout is used to stop downloading images which take too long to download
                timeout(testfile.retrieve, (line.split()[4], "uncropped/"+filename), {}, 1)
                if not os.path.isfile("uncropped/"+filename):
                    continue
                try:
                    if chopped:
                        img = imread("uncropped/"+filename)
                        pos = line.split()[5].split(',')
                        for j in range(len(pos)):
                            pos[j] = int(pos[j])
                        #print img.shape
                        #print pos
                        chopped_img = img[pos[1]:pos[3], pos[0]:pos[2]]
                        resized_img = imresize(chopped_img,(32, 32))
                        #print resized_img.shape
                        
                        imsave("cropped/"+filename, rgb2gray(resized_img))
                except Exception as e:
                    print e
                    i -= 1
                print filename
                i += 1
        f = open('uncropped/track.txt', 'r+')   
        f.write(names + ',' + name) #record finshed actor

def cross_validation(arr, size=120, mode='name', dirt="cropped/", split=12):
    train_data = []
    valid_data = []
    test_data = []
    train_target = []
    valid_target = []
    test_target = []
    for a in arr:
        name = a.split()[1].lower()
        if name == 'gray' or name == 'marie':
            continue
        if mode == 'name':
            target = np.zeros(0,6)
            target[enum[name]] = 1

        for i in range(size):
            if os.path.isfile(dirt+name+str(i)+'.jpg'):
                path = dirt+name+str(i)+'.jpg'
            elif os.path.isfile(dirt+name+str(i)+'.jpeg'):
                path = dirt+name+str(i)+'.jpeg'
            elif os.path.isfile(dirt+name+str(i)+'.png'):
                path = dirt+name+str(i)+'.png'
            else:
                print name + str(i) + 'is not found'
                zero = np.zeros((32,32))
                if i % split == 0: #test
                    test_data.append(zero)
                    test_target.append(target)
                elif i % split == 1: #validation
                    valid_data.append(zero)
                    valid_target.append(target)
                else:
                    train_data.append(zero)
                    train_target.append(target)
                continue
            #print path
            #imshow(imread(path))
            #a =raw_input('a')
            if i % split == 0: #test
                test_data.append(imread(path))
                test_target.append(target)
            elif i % split == 1: #validation
                valid_data.append(imread(path))
                valid_target.append(target)
            else:
                train_data.append(imread(path))
                train_target.append(target)
            #print train_target
            #print np.asarray(train_data)

    return np.asarray(train_data).astype(float), np.asarray(valid_data).astype(float),np.asarray(test_data).astype(float),np.asarray(train_target), np.asarray(valid_target), np.asarray(test_target)

if __name__ == '__main__':
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    download(sub)
