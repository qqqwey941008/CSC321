from scipy.io import loadmat
from pylab import *
from numpy import *
import matplotlib.pyplot as plt


def part1(data):
	for i in range(10):
		key = 'train' + str(i)
		for j in range(4):
			filename = 'digit' + str(i)
			row = data[key][j*4].reshape((28,28))
			for k in range(2):
				row = hstack((row, data[key][j*4+k+1].reshape((28,28))))
			if j == 0:
				img = row
			else:
				img = vstack((img, row))
			print img.shape
		imsave("img/p1/"+filename, img)		

def part2(w, x, b):
	'''
	w 784*10
	x 1*784
	b 10*1
	'''
	return softmax(dot(w.T, x.T) + b)

def softmax(a):
	return exp(a)/tile(sum(exp(a),0), (len(a),1))

def part3(x, p, y):
	'''
	p 10*1
	y 10*1
	x 1*784
	dC/dW 784*10
	dC/dB 10*1
	'''
	dCdO = p - y
	dCdB = dCdO

	dOdW = x
	dCdW = dot(dCdO, dOdW)
	return dCdW.T, dCdB

def cost_function(p, y):
	return -sum(y*log(p))

def part4(change=0.0001):
	x = random.rand(1, 784)
	b = random.rand(10, 1)
	w = random.rand(784, 10)
	p = part2(w, x, b)
	y = array([[1,0,0,0,0,0,0,0,0,0]]).T
	
	dCdW, dCdB = part3(x, p, y)
	
	dCdW1 = zeros(dCdW.shape)
	dCdB1 = zeros(dCdB.shape)
	temp = zeros(dCdW1.shape)
	
	for i in range(len(dCdW1)):
		for j in range(len(dCdW1[i])):
			temp[i][j] += change
			dCdW1[i][j] = (cost_function(part2(w+temp,x,b),y) - cost_function(part2(w-temp, x, b),y))/(2*change)
			temp[i][j] -= change
			
	temp = zeros(dCdB1.shape)	
	for i in range(len(dCdB1)):
		for j in range(len(dCdB1[i])):
			temp[i][j] += change
			dCdB1[i][j] = (cost_function(part2(w, x, b+temp),y) - cost_function(part2(w, x, b-temp),y))/(2*change)
			temp[i][j] -= change

	print norm(dCdW1-dCdW)/norm(dCdW1+dCdW)
	print norm(dCdB1-dCdB)/norm(dCdB1+dCdB)
	
def part5(input_data, input_target, valid_data, valid_target, learning_rate=0.01, batch_size=50, epoch=1000):
	w = random.rand(784, 10) / 100
	b = random.rand(10, 1) / 100
	for i in range(epoch):
		print 'epoch: ' + str(i)
		dW = zeros(w.shape)
		dB = zeros(b.shape)
		for j in range(batch_size):
			page = i * batch_size
			o = part2(w, input_data[page + j].reshape(1,784), b)
			dCdW, dCdB = part3(input_data[page + j].reshape(1,784), o, input_target[page + j].reshape(10,1))
			dW += dCdW
			dB += dCdB
		w -= learning_rate * dW
		b -= learning_rate * dB
		p = part2(w, valid_data, b)
		print 'classification rate: ' + str(classification(valid_target, p))
	return w, b

def classification(y, p):
	return sum(argmax(y,axis=1) == argmax(p.T,axis=1)) / (0.0 + len(y))

def part6(w, dir="img/p6/"):
	count = 0
	for each in w:
		imsave(dir+str(count), each.reshape((28,28)))
		count += 1


def tanh_layer(y, W, b):    
	'''Return the output of a tanh layer for the input matrix y. y
    is an NxM matrix where N is the number of inputs for a single case, and M
    is the number of cases'''
	return tanh(dot(W.T, y.T) + b)

def forward(x, W0, b0, W1, b1):
	L0 = tanh_layer(x, W0, b0)
	L1 = tanh_layer(L0, W1, b1)
	output = softmax(L1)
	return L0, L1, output

def complex_network(x, w0, b0, w1, b1):
	'''
	w0 784*300
	b0 300*1
	w1 300*10
	b1 10*1
	x 1*784
	'''
	l0 = tanh_layer(x, w0, b0)
	l1 = dot(w1.T, l0) + b1
	return softmax(l1)

def part7(x, w0, b0, l0, w1, b1, l1, p, y):
	dCdL1 = p - y
	dCdW1 = dot(dCdL1, l0.T)
	dCdB1 = dCdL1.T
	dCdL0 = dot(dCdL1.T, w1.T)
	dCdW0 = dot((1- l0**2)*dCdL0.T, x)
	dCdB0 = ((1- l0**2)*dCdL0.T)
	return dCdW0.T, dCdB0, dCdW1.T, dCdB1.T

def part8(change=0.0001):
	x = random.rand(1, 784) * change
	b0 = random.rand(300, 1) * change
	w0 = random.rand(784, 300) * change
	b1 = random.rand(10, 1) * change
	w1 = random.rand(300, 10) * change
	p = complex_network(x, w0, b0, w1, b1)
	y = array([[1,0,0,0,0,0,0,0,0,0]]).T	
	l0 = tanh_layer(x, w0, b0)
	l1 = dot(w1.T, l0) + b1
	
	dCdW0, dCdB0, dCdW1, dCdB1 = part7(x, w0, b0, l0, w1, b1, l1, p, y)
	dCdW0_ = zeros(dCdW0.shape)
	dCdB0_ = zeros(dCdB0.shape)
	dCdW1_ = zeros(dCdW1.shape)
	dCdB1_ = zeros(dCdB1.shape)

	temp = zeros(dCdB0.shape)
	for i in range(len(dCdB0)):
		for j in range(len(dCdB0[i])):
			temp[i][j] += change
			dCdB0_[i][j] = (cost_function(complex_network(x, w0, b0 + temp, w1, b1),y) - cost_function(complex_network(x, w0, b0 - temp, w1, b1),y))/(2*change)
			temp[i][j] -= change	
	print norm(dCdB0_-dCdB0)/norm(dCdB0_+dCdB0)
	
	temp = zeros(dCdW1.shape)
	for i in range(len(dCdW1)):
		for j in range(len(dCdW1[i])):
			temp[i][j] += change
			dCdW1_[i][j] = (cost_function(complex_network(x, w0, b0, w1 + temp, b1),y) - cost_function(complex_network(x, w0, b0, w1 + temp, b1),y))/(2*change)
			temp[i][j] -= change
	print norm(dCdW1_-dCdW1)/norm(dCdW1_+dCdW1)
	
	temp = zeros(dCdB1.shape)
	for i in range(len(dCdB1)):
		for j in range(len(dCdB1[i])):
			temp[i][j] += change
			dCdB1_[i][j] = (cost_function(complex_network(x, w0, b0, w1, b1 + temp),y) - cost_function(complex_network(x, w0, b0, w1, b1 - temp),y))/(2*change)
			temp[i][j] -= change
	print norm(dCdB1_-dCdB1)/norm(dCdB1_+dCdB1)
	
	temp = zeros(dCdW0.shape)
	count = 0
	for i in range(len(dCdW0)):
		for j in range(len(dCdW0[i])):
			temp[i][j] += change
			dCdW0_[i][j] = (cost_function(complex_network(x, w0 + temp, b0, w1, b1),y) - cost_function(complex_network(x, w0 - temp, b0, w1, b1),y))/(2*change)
			temp[i][j] -= change
			count += 1
	print norm(dCdW0_-dCdW0)/norm(dCdW0_+dCdW0)	

def part9(input_data, input_target, valid_data, valid_target, learning_rate=0.01, batch_size=50, epoch=1000):
	w0 = random.rand(784, 300) / 100
	b0 = random.rand(300, 1) / 100
	w1 = random.rand(300, 10) / 100
	b1 = random.rand(10, 1) / 100
	for i in range(epoch):
		print 'epoch: ' + str(i)
		dW0 = zeros(w0.shape)
		dB0 = zeros(b0.shape)
		dW1 = zeros(w1.shape)
		dB1 = zeros(b1.shape)
		
		for j in range(batch_size):
			page = i * batch_size
			x = input_data[page + j].reshape(1,784)
			
			p = complex_network(x, w0, b0, w1, b1)
			l0 = tanh_layer(x, w0, b0)
			l1 = dot(w1.T, l0) + b1	
			dCdW0, dCdB0, dCdW1, dCdB1 = part7(x, w0, b0, l0, w1, b1, l1, p, input_target[page + j].reshape(10,1))
			dW0 += dCdW0
			dB0 += dCdB0
			dW1 += dCdW1
			dB1 += dCdB1
			
		w0 -= learning_rate * dW0
		b0 -= learning_rate * dB0
		w1 -= learning_rate * dW1
		b1 -= learning_rate * dB1
		p = complex_network(valid_data, w0, b0, w1, b1)
		print 'classification rate: ' + str(classification(valid_target, p))
	return w0.T, b0, w1, b1

def preprocess(data):
	temp = identity(10)
	for i in range(10):
		if i == 0:
			input_data = M['train' + str(i)]
			valid_data = M['test' + str(i)]
			input_target = zeros((M['train' + str(i)].shape[0],10)) + temp[i]
			valid_target = zeros((M['test' + str(i)].shape[0],10)) + temp[i]
		else:
			input_data = vstack((input_data, M['train' + str(i)]))
			valid_data = vstack((valid_data, M['test' + str(i)]))
			input_target = vstack((input_target, zeros((M['train' + str(i)].shape[0],10)) + temp[i]))
			valid_target = vstack((valid_target, zeros((M['test' + str(i)].shape[0],10)) + temp[i]))
	final_input_data = zeros(input_data.shape)
	final_input_target = zeros(input_target.shape)
	for i in range(input_data.shape[0]):
		final_input_data[i] = input_data[i%10*6000 + i//10]
		final_input_target[i] = input_target[i%10*6000 + i//10]
	return final_input_data / 255.0, final_input_target, valid_data / 255.0, valid_target
if __name__ == '__main__':
	M = loadmat("mnist_all.mat")
	input_data, input_target, valid_data, valid_target = preprocess(M)
	#part1(M)
	#part4()
	#w, b = part5(input_data, input_target, valid_data, valid_target)
	#part6(w.T)
	#part8()
	w0, b0, w1, b1 = part9(input_data, input_target, valid_data, valid_target)
	part6(w0, 'img/p10')
