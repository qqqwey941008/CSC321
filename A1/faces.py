from pylab import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
import random
import time
from scipy.misc import imread
from scipy.misc import imresize
from scipy.misc import imsave
import matplotlib.image as mpimg
import os
from scipy.ndimage import filters
import urllib
import preprocess

male = list(set([a.split("\t")[0] for a in open("subset_actors.txt").readlines()]))
female = list(set([a.split("\t")[0] for a in open("subset_actresses.txt").readlines()]))

def timeout(func, args=(), kwargs={}, timeout_duration=1, default=None):
    '''From:
    http://code.activestate.com/recipes/473878-timeout-function-using-threading/'''
    import threading
    class InterruptableThread(threading.Thread):
        def __init__(self):
            threading.Thread.__init__(self)
            self.result = None

        def run(self):
            try:
                self.result = func(*args, **kwargs)
            except:
                self.result = default

    it = InterruptableThread()
    it.start()
    it.join(timeout_duration)
    if it.isAlive():
        return False
    else:
        return it.result

def download(act, chopped=True, num=3):
    testfile = urllib.URLopener()
    f = open('uncropped/track.txt', 'r+')
    names = f.readline()
    f.close()
    all_names = names.split(',')
    #Note: you need to create the uncropped folder first in order
    #for this to work

    for a in act:
        print a
        name = a.split()[1].lower()
        if name in all_names:
            continue
        i = 0
        for line in open("faces_subset.txt"):
            if a in line and i < num:
                filename = name+str(i)+'.'+line.split()[4].split('.')[-1]
                #A version without timeout (uncomment in case you need to
                #unsupress exceptions, which timeout() does)
                #testfile.retrieve(line.split()[4], "uncropped/"+filename)
                #timeout is used to stop downloading images which take too long to download
                timeout(testfile.retrieve, (line.split()[4], "uncropped/"+filename), {}, 1)
                if not os.path.isfile("uncropped/"+filename):
                    continue
                try:
                    if chopped:
                        img = imread("uncropped/"+filename)
                        pos = line.split()[5].split(',')
                        for j in range(len(pos)):
                            pos[j] = int(pos[j])
                        #print img.shape
                        #print pos
                        chopped_img = img[pos[1]:pos[3], pos[0]:pos[2]]
                        resized_img = imresize(chopped_img,(32, 32))
                        #print resized_img.shape

                        imsave("cropped/"+filename, rgb2gray(resized_img))
                except:
                    i -= 1
                print filename
                i += 1
        f = open('uncropped/track.txt', 'r+')
        f.write(names + ',' + name) #record finshed actor


def rgb2gray(rgb):
    '''Return the grayscale version of the RGB image rgb as a 2D numpy array
    whose range is 0..1
    Arguments:
    rgb -- an RGB image, represented as a numpy array of size n x m x 3. The
    range of the values is 0..255
    '''

    r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
    gray = 0.2989 * r + 0.5870 * g + 0.1140 * b

    return gray/255.

def cross_validation(arr, size=120, mode='name', dirt="cropped/", split=12):
    train_data = []
    valid_data = []
    test_data = []
    train_target = []
    valid_target = []
    test_target = []
    for a in arr:
        name = a.split()[1].lower()
        if name == 'gray' or name == 'marie':
            continue
        if mode == 'name':
            target = name
        elif mode == 'gender':
            target = 0
            if a in male:
                target = 1

        for i in range(size):
            if os.path.isfile(dirt+name+str(i)+'.jpg'):
                path = dirt+name+str(i)+'.jpg'
            elif os.path.isfile(dirt+name+str(i)+'.jpeg'):
                path = dirt+name+str(i)+'.jpeg'
            elif os.path.isfile(dirt+name+str(i)+'.png'):
                path = dirt+name+str(i)+'.png'
            else:
                print name + str(i) + 'is not found'
                zero = np.zeros((32,32)).reshape(32*32)
                if i % split == 0: #test
                    test_data.append(zero)
                    test_target.append(target)
                elif i % split == 1: #validation
                    valid_data.append(zero)
                    valid_target.append(target)
                else:
                    train_data.append(zero)
                    train_target.append(target)
                continue
            #print path
            #imshow(imread(path))
            #a =raw_input('a')
            if i % split == 0: #test
                test_data.append(imread(path).reshape(32*32))
                test_target.append(target)
            elif i % split == 1: #validation
                valid_data.append(imread(path).reshape(32*32))
                valid_target.append(target)
            else:
                train_data.append(imread(path).reshape(32*32))
                train_target.append(target)
            #print train_target
            #print np.asarray(train_data)

    return np.asarray(train_data).astype(float), np.asarray(valid_data).astype(float),np.asarray(test_data).astype(float),np.asarray(train_target), np.asarray(valid_target), np.asarray(test_target)

def knn(k, train_data, train_target, valid_data):
    distance = euclidean_distance(train_data, valid_data)
    #print distance[0]
    #print np.argsort(distance)[:,:k]
    #print distance.shape
    #print np.argsort(distance)[0][0]
    #print distance[0][np.argsort(distance)[0][0]]
    index = np.argsort(distance)
    top_k = train_target[np.argsort(distance)[:,:k]]
    target = []
    for row in top_k:
        #print row
        #a = raw_input('a')
        dct = {}
        maxT = ''
        maxnum = 0
        for t in row:
            if t in dct:
                dct[t] += 1
            else:
                dct[t] = 1
            if maxnum <= dct[t]:
                maxnum = dct[t]
                maxT = t
        target.append(maxT)
    return np.asarray(target)

def classifaction(target, valid_target):
    count = 0
    for i in range(len(valid_target)):
        if target[i] == valid_target[i]:
            count += 1
    return (count + 0.0)/len(target)

def euclidean_distance(a, b):
    a2 = sum(a ** 2, axis = 1) #a^2 N*1
    b2 = sum(b ** 2, axis = 1) #b^2 M*1
    ab = np.dot(a, b.T) #a*b NM
    # sqrt((a-b)^2) = sqrt(a^2 + b^2 - 2ab)
    for i in range(len(ab)):
        for j in range(len(ab[0])):
            ab[i][j] = -2 * ab[i][j] + a2[i] + b2[j]
    return sqrt(ab.T)

def part3a():
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    train_data, valid_data, test_data, train_target, valid_target, test_target = cross_validation(sub)
    best_k = -1
    best_rate = 0
    for i in range(1, 15, 2):
        predict = knn(i, train_data, train_target, valid_data)
        classificaton_rate = classifaction(predict, valid_target)
        if classificaton_rate > best_rate:
            best_rate = classificaton_rate
            best_k = i
        print 'for k=' + str(i) + ', classification rate is ' + str(classificaton_rate)

    predict = knn(best_k, train_data, train_target, test_data)
    classificaton_rate = classifaction(predict, test_target)
    print 'K used for test set is ' + str(best_k) + ', classification rate is ' + str(classificaton_rate)

def part3b():
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    train_data, valid_data, test_data, train_target, valid_target, test_target = cross_validation(sub)    
    #===================================================================
    #find 5 failed case
    predict = knn(5, train_data, train_target, valid_data)
    bad_cases = []
    for i in range(len(predict)):
        if predict[i] != valid_data[i]:
            bad_cases.append(i)
        if len(bad_cases) >= 5:
            break
    distance = euclidean_distance(train_data, valid_data)
    index = np.argsort(distance)[:,:5]
    for i in bad_cases:
        imsave('failed/valid' + str(i) + '.jpg', valid_data[i].reshape(32,32))
        #imshow(valid_data[i].reshape(32,32),cmap=plt.get_cmap('gray'))
        count = 0
        for j in index[i]:
            count += 1
            imsave('failed/failed' + str(i) + str(count) + '.jpg', train_data[j].reshape(32,32))



def part4():
    graph = []
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    train_data, valid_data, test_data, train_target, valid_target, test_target = cross_validation(sub)
    for i in range(1, 16, 2):
        predict = knn(i, train_data, train_target, valid_data)
        classificaton_rate = classifaction(predict, valid_target)
        graph.append(classificaton_rate * 100)

    a, = plt.plot([1,3,5,7,9,11,13,15], graph, label='Validation')

    graph = []
    for i in range(1, 16, 2):
        predict = knn(i, train_data, train_target, test_data)
        classificaton_rate = classifaction(predict, test_target)
        graph.append(classificaton_rate * 100)

    b, = plt.plot([1,3,5,7,9,11,13,15], graph, label='Test')
    plt.legend(handles=[a,b])
    plt.axis([1, 15, 50, 70])
    plt.title('Classification Rate for various K')
    plt.xlabel('K value')
    plt.ylabel('Rate %')
    plt.show()


def part5():
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    train_data, valid_data, test_data, train_target, valid_target, test_target = cross_validation(sub, mode='gender')
    best_k = -1
    best_rate = 0
    for i in range(1, 15, 2):
        predict = knn(i, train_data, train_target, valid_data)
        classificaton_rate = classifaction(predict, valid_target)
        if classificaton_rate > best_rate:
            best_rate = classificaton_rate
            best_k = i
        print 'for k=' + str(i) + ', classification rate is ' + str(classificaton_rate)

    predict = knn(best_k, train_data, train_target, test_data)
    classificaton_rate = classifaction(predict, test_target)
    print 'K used for test set is ' + str(best_k) + ', classification rate is ' + str(classificaton_rate)

def part6():
    sub = ['Gerard Butler', 'Daniel Radcliffe', 'Michael Vartan', 'Lorraine Bracco', 'Peri Gilpin', 'Angie Harmon']
    train_data, valid_data, test_data, train_target, valid_target, test_target = cross_validation(sub, mode='gender')
    sadkjfadkj, valid_data, test_data, sdssd, valid_target, test_target = cross_validation(male + female, mode='gender', dirt="cropped/", split=2, size=20)
    best_k = -1
    best_rate = 0
    for i in range(1, 15, 2):
        predict = knn(i, train_data, train_target, valid_data)
        classificaton_rate = classifaction(predict, valid_target)
        if classificaton_rate > best_rate:
            best_rate = classificaton_rate
            best_k = i
        print 'for k=' + str(i) + ', classification rate is ' + str(classificaton_rate)
    predict = knn(best_k, train_data, train_target, test_data)
    classificaton_rate = classifaction(predict, test_target)
    print 'K used for test set is ' + str(best_k) + ', classification rate is ' + str(classificaton_rate)

def test_case():
    a = np.array([[0,1,1], [0,2,2], [0,3,3], [-1,-1,-1]])
    b = np.array([[0,2,2], [0,100,100], [0,0,0]])
    c = np.array(['a', 'b', 'c', 'd'])
    d = np.array(['b', 'c', 'a'])
    print euclidean_distance(a, b)
    print knn(1, a, c, b)
    print classifaction(knn(1, a, c, b), d)
    '''
    print count_nonzero(euclidean_distance(train_data, valid_data))
    print knn(10, train_data, train_target, test_data)[:10]
    print valid_target[:20]
    '''

if __name__ == '__main__':
    part3a()
    part3b() #require failed folder
    part4()
    part5()
    part6()
    pass